## [0.0.4](https://gitlab.com/dmoonfire/commitlint-gitlab-ci/compare/v0.0.3...v0.0.4) (2021-07-25)


### Bug Fixes

* **npm:** cleaning up NPM package ([a706aa1](https://gitlab.com/dmoonfire/commitlint-gitlab-ci/commit/a706aa1ce26be67cad410ebdeb0646ef946b6d19))

## [0.0.3](https://gitlab.com/dmoonfire/commitlint-gitlab-ci/compare/v0.0.2...v0.0.3) (2021-07-25)


### Bug Fixes

* reduced verbosity ([f1c6214](https://gitlab.com/dmoonfire/commitlint-gitlab-ci/commit/f1c62140e3dec950320877cc83944f876683f42e))

## [0.0.1](https://gitlab.com/dmoonfire/commitlint-gitlab-ci/compare/v0.0.0...v0.0.1) (2021-07-25)


### Bug Fixes

* **commitlint:** adding git back in ([65032ba](https://gitlab.com/dmoonfire/commitlint-gitlab-ci/commit/65032baafac9b3f12c592f3a579759c708ff9656))
