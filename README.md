commitlint-gitlab-ci
====================

Beyond a doubt, [commitlint](https://commitlint.js.org/#/) is a useful tool for working with verifying a Git history and keeping everything clean, more so when working with [semantic-release](https://semantic-release.gitbook.io/semantic-release/).

When working with [Gitlab](https://gitlab.com/), `commitlint` has a [minor issue](https://github.com/conventional-changelog/commitlint/issues/885) when run for the first time or when force push is used. The fix is the following:

```sh
if [ "${CI_BUILD_BEFORE_SHA}" = "0000000000000000000000000000000000000000" ]; then
  echo "commitlint from HEAD^"
  npx commitlint -x @commitlint/config-conventional -f HEAD^
else
  echo "commitlint from ${CI_BUILD_BEFORE_SHA}"
  br=`git branch -r --contains ${CI_BUILD_BEFORE_SHA}`
  if [ ! -n $br ]; then
    npx commitlint -x @commitlint/config-conventional -f HEAD^
  else
    npx commitlint -x @commitlint/config-conventional -f "${CI_BUILD_BEFORE_SHA}"
  fi
fi
```

To make it easier to do the above, this package was created to consolidate it down to a single line:

```sh
npx commitlint-gitlab-ci -x @commitlint/config-conventional
```

Any parameter can be used, this just wraps it around the call the various `if` statements needed to work with Gitlab.
